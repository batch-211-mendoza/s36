// Import packages
const express = require("express");

// Middleware
const router = express.Router();

// Import controller
const taskController = require("../controllers/taskControllers");

// Routes
/*
	- Routes are responsible for defining the URIs (Uniform Resource Identifier) that our client accesses and the corresponding controller functions that will be used when a route is accessed.
*/

// Route to get all the tasks
/*
	- This route expects to receive a GET request at the URL "/tasks"
	- The whole URL is at "http://localhost:3001/tasks"
*/
router.get("/", (request, response) => {
	taskController.getAllTasks().then(resultFromController => {
		response.send(resultFromController)
	});
});

router.post("/", (request, response) => {
	taskController.createTask(request.body).then(resultFromController => {
		response.send(resultFromController);
	});
});

// Route to delete a task
/*
	- Expects to receive a DELETE request at the URL "tasks/:id"
	- The whole URL is http://localhost:3001/tasks/:id
*/

router.delete("/:id", (request, response) => {
	taskController.deleteTask(request.params.id).then(resultFromController => {
		response.send(resultFromController);
	});
});

router.put("/:id", (request, response) => {
	taskController.updateTask(request.params.id, request.body).then(resultFromController => {
		response.send(resultFromController);
	});
})

// ----- ACTIVITY ----- //

// 1.
router.get("/:id", (request, response) => {
	taskController.getSpecificTask(request.params.id).then(resultFromController => {
		response.send(resultFromController)
	});
});

// 2. 
router.patch("/:id/complete", (request, response) => {
	taskController.updateTaskStatus(request.params.id).then(resultFromController => {
		response.send(resultFromController);
	});
});


// use "module.exports" to export the router object to use in the index.js
module.exports = router;