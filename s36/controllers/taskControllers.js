// Import packages
const Task = require("../models/Task");

// Controllers
/*
	- Controllers contain the functions and business logic of our Express.js Application
	- Meaning all the operations it can do will be placed in this file
*/

// Controller function for getting all the tasks

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	});
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		} else{
			return task;
		}
	});
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if(error){
			console.log(error);
			return false;
		} else{
			return removedTask;
		}
	});
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error)=> {
		if(error){
			console.log(error);
			return false;
		}
		result.name = newContent.name;
		return result.save().then((updatedTask, saveError) => {
			if(saveError){
				console.log(saveError);
				return false;
			} else{
				return updatedTask;
			}
		});
	});
}

// ----- ACTIVITY ----- //

// 1. 
module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then((foundTask, error) => {
		if(error){
			console.log(error);
			return false;
		} else{
			return foundTask;
		}
	});
}

// 2.
module.exports.updateTaskStatus = (taskId, statusUpdate) => {
	return Task.findById(taskId).then((result, error)=> {
		if(error){
			console.log(error);
			return false;
		}
		result.status = "completed";
		return result.save().then((updatedTaskStatus, saveError) => {
			if(saveError){
				console.log(saveError);
				return false;
			} else{
				return updatedTaskStatus;
			}
		});
	});
}