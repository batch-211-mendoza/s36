// Import packages
const mongoose = require("mongoose");

// Create the schema, model, and export the file

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

module.exports = mongoose.model("Task", taskSchema);